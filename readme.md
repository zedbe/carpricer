# CarPricer
## Introduction
This git project is homebase for my diploma thesis. Goal of thesis is to make prediction of car prices in future, based on collected data and deep learning in python. 

My personal desire is to make a web flask app where user select his dream/wanted car and app will predict his price in future. App would be dockerized and basic-functionality covered by unit tests. 

## Used techs
- Keras, Python 3.6
- time series artificial neural networks
- ...